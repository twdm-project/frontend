package com.example.twdmapp;

import android.os.Bundle;
import android.widget.ExpandableListView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class InterfacesActivity extends AppCompatActivity {

    ExpandableListView expandableListView;

    List<String> listGroup;

    HashMap<String, List<String>> listItem;

    MainAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_interfaces);

        Bundle bundle = getIntent().getExtras();
        int size = bundle.getInt("size");
        System.out.println(size);


        expandableListView = findViewById(R.id.expandable_listview);
        listGroup = new ArrayList<>();
        listItem = new HashMap<>();
        adapter = new MainAdapter(this, listGroup, listItem);

        for(int i = 0; i < 3; i++) {
            String name = bundle.getString("displayName" + i);
            String addresses = bundle.getString("addresses" + i);
            String virtual = bundle.getString("virtual" + i);
            String mtu = bundle.getString("mtu" + i);
            String loopback = bundle.getString("loopback" + i);
            String pointToPoint = bundle.getString("pointToPoint" + i);
            String multicast = bundle.getString("multicast" + i);

            List<String> details = new ArrayList<>();
            details.add(addresses);
            details.add(virtual);
            details.add(mtu);
            details.add(loopback);
            details.add(pointToPoint);
            details.add(multicast);

            initListData(name, details, i);
        }

        expandableListView.setAdapter(adapter);
        adapter.notifyDataSetChanged();

    }

    private  void initListData(String name, List<String> details, int i) {
        listGroup.add(name);

        listItem.put(listGroup.get(i), details);
    }
}
