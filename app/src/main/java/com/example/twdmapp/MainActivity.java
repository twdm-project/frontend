package com.example.twdmapp;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.common.base.Splitter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;


public class MainActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RequestQueue requestQueue = Volley.newRequestQueue(this);

        final Button button = (Button) findViewById(R.id.button2);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Button 2");

                requestQueue.add(getUserInterfaces());

            }
        });

        final Button button2 = (Button) findViewById(R.id.button3);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Button 3");

                requestQueue.add(getUserInterfacesWithDetails());

            }
        });
    }


    private void openNewActivity(JSONObject interfaces) throws JSONException {
        Intent intent = new Intent(this, NewActivity.class);
        intent.putExtra("size", "" + interfaces.length());
        for(int i = 0; i < interfaces.length(); i++) {
            intent.putExtra("" + i, interfaces.getString(""+i));
        }
        startActivity(intent);
    }


    public JsonObjectRequest getUserInterfaces() {
        String URL = "http://192.168.0.105:8080/getAllInterfaces";

        JsonObjectRequest objectRequest = new JsonObjectRequest(
                Request.Method.GET,
                URL,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            openNewActivity(response);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Rest Response", error.toString());
                    }
                }
        );

        return objectRequest;
    }

    private void openInterfaceActivity(List<String> interfaces) throws JSONException {
        Intent intent = new Intent(this, InterfacesActivity.class);
        intent.putExtra("size", interfaces.size());
        for(int i = 0; i < interfaces.size(); i++) {
            List<String> list = Splitter.on(',').splitToList(interfaces.get(i));

            intent.putExtra("displayName" + i, list.get(1).split(":")[1]);
            intent.putExtra("addresses" + i, list.get(5) + list.get(6));
            intent.putExtra("virtual" + i, list.get(8));
            intent.putExtra("mtu" + i, list.get(9));
            intent.putExtra("loopback" + i, list.get(10));
            intent.putExtra("pointToPoint" + i, list.get(11));
            intent.putExtra("multicast" + i, list.get(12));
        }

        startActivity(intent);
    }

    public JsonObjectRequest getUserInterfacesWithDetails() {
        String URL = "http://192.168.0.105:8080/getInterfaces";

        JsonObjectRequest objectRequest = new JsonObjectRequest(
                Request.Method.GET,
                URL,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            List<String> list = new ArrayList<>();

                            for(int i = 0; i < 100; i++) {
                                if( response.has("" + i) ) {
                                    list.add(response.get("" + i).toString());
                                }
                            }

                            openInterfaceActivity(list);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Rest Response", error.toString());
                    }
                }
        );

        return objectRequest;
    }
}