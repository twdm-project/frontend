package com.example.twdmapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import android.os.Bundle;


import com.trendyol.bubblescrollbarlib.BubbleScrollBar;
import com.trendyol.bubblescrollbarlib.BubbleTextProvider;

import java.util.ArrayList;
import java.util.List;

public class NewActivity extends AppCompatActivity {

    RecyclerView recyclerView;

    BubbleScrollBar bubbleScrollBar;

    List<String> interfaces;

    NameAdapter nameAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        Bundle bundle = getIntent().getExtras();
        String message = bundle.getString("size");

        interfaces = new ArrayList<>();
        for(int i = 0; i < Integer.valueOf(message); i++) {
            interfaces.add(" " + i + ". " + bundle.getString(""+i));
        }

        recyclerView = findViewById(R.id.recyclerView);
        bubbleScrollBar = findViewById(R.id.bubbleScroll);

        nameAdapter = new NameAdapter(interfaces, this);
        recyclerView.setAdapter(nameAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        bubbleScrollBar.attachToRecyclerView(recyclerView);
        bubbleScrollBar.setBubbleTextProvider(new BubbleTextProvider() {
            @Override
            public String provideBubbleText(int i) {
                return nameAdapter.list.get(i);
            }
        });
    }
}
