package com.example.twdmapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;


import java.util.Collections;
import java.util.List;

public class NameAdapter extends RecyclerView.Adapter<NameHolder> {
    Context context;
    List<String> list = Collections.emptyList();

    public NameAdapter(List<String> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public NameHolder onCreateViewHolder( ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.name_row, parent, false);
        NameHolder nameHolder = new NameHolder(view);
        return nameHolder;
    }

    @Override
    public void onBindViewHolder( NameHolder holder, int position) {
        holder.textView.setText(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
}
