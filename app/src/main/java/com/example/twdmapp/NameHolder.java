package com.example.twdmapp;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


public class NameHolder extends RecyclerView.ViewHolder {

    TextView textView;

    public NameHolder(View itemView) {
        super(itemView);
        textView = itemView.findViewById(R.id.tvName);
    }
}
